// COMPARISON QUERY OPERATORS

/*
	- greater than && greater than or equal to
	- $gt/gte operator
*/
	db.users.find({age: {$gt: 50}})
	db.users.find({age: {$gte: 50}})

/*
	- less than && less than or equal to
	- $lt/lte operator
*/
	db.users.find({age: {$lt: 50}})
	db.users.find({age: {$lte: 50}})

/*
	- not equal
	- $ne operator
*/
	db.users.find({ age: {$ne: 82} })

/*
	- include
	- $in operator is used to select documents in which the field's value equals 
	any of the given values in the array
*/
	db.users.find({ lastName: {$in: ["Hawking", "Rizal"]} })
	db.users.find({ courses: {$in: ["php", "laravel"]} })


// LOGICAL QUERY OPERATORS

/*
	$or operator - performs a logical OR operation on an array of one or 
	more <expressions> and selects the documents that satisfy at least one
*/
	db.users.find({$or: [{firstName: "Jose"}, {age: 80}]})

/*
	$and operator - used to perform logical AND operation on the array of 
	one or more expressions and select or retrieve only those documents 
	that match all the given expression in the array
*/
	db.users.find({$and: [{firstName: "Jose"}, {age: 162}]})


// FIELD PROJECTION

/*
	Inclusion - includes field that you want to retrieve
*/
	db.users.find(
		{
			firstName: "Rene"
		},
		{
			firstName: 1,
			lastName: 1,
			contact: 1
		}
	)
	// finding embedded fields
	db.users.find(
		{
			firstName: "Rene"
		},
		{
			firstName: 1,
			lastName: 1,
			"contact.phone": 1
		}
	)

/*
	Exclusion - excludes field that you dont want to retrive
*/
	db.users.find(
		{
			firstName: "Rene"
		},
		{
			contact: 0,
			department: 0
		}
	)

	db.users.find(
		{
			firstName: "Rene"
		},
		{
			_id: 0,
			contact: 1
		}
	)

/*
	Mini-activity - find all fields except the embedded phone field in contact field
*/

db.users.find(
	{
		firstName: "Jose"
	},
	{
		"contact.phone": 0
	}
)


//	Evaluation query operators

/*
	$regex operator - find documents that match a specific pattern using 
	regular expressions
*/

db.users.find({firstName: {$regex: 'a', $options: '$i'}})